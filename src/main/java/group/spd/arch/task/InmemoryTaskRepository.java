package group.spd.arch.task;

import java.util.ArrayList;
import java.util.List;

public class InmemoryTaskRepository implements TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task save(Task task) {
        task.setId(tasks.size() + 1);
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> getAll() {
        return new ArrayList<>(tasks);
    }
}
