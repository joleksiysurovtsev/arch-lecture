package group.spd.arch.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import group.spd.arch.DependencyLocator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/tasks")
public class ServletTaskController extends HttpServlet {

    private ObjectMapper om;
    private TaskService taskService;

    @Override
    public void init() {
        om = DependencyLocator.INSTANCE.getObjectMapper();
        taskService = DependencyLocator.INSTANCE.getTaskService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final List<Task> tasks = taskService.getAll();

        resp.setContentType("application/json");
        om.writeValue(resp.getOutputStream(), tasks);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final Task task = om.readValue(req.getInputStream(), Task.class);
        final Task createdTask = taskService.save(task);

        resp.setContentType("application/json");
        om.writeValue(resp.getOutputStream(), createdTask);
    }
}
