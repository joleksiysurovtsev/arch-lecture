package group.spd.arch.person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InmemoryPersonRepository implements PersonRepository, PersonProvider {

    private final List<Person> persons = new ArrayList<>();

    @Override
    public Optional<Person> getById(int id) {
        return persons.stream()
                .filter(person -> person.getId() == id)
                .findAny();
    }

    @Override
    public Person save(Person person) {
        person.setId(persons.size() + 1);
        persons.add(person);
        return person;
    }

    @Override
    public List<Person> getAll() {
        return new ArrayList<>(persons);
    }
}
