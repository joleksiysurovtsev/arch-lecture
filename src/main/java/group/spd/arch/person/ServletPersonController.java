package group.spd.arch.person;

import com.fasterxml.jackson.databind.ObjectMapper;
import group.spd.arch.DependencyLocator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/persons")
public class ServletPersonController extends HttpServlet {

    private ObjectMapper om;
    private PersonService personService;

    @Override
    public void init() {
        om = DependencyLocator.INSTANCE.getObjectMapper();
        personService = DependencyLocator.INSTANCE.getPersonService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final List<Person> persons = personService.getAll();

        resp.setContentType("application/json");
        om.writeValue(resp.getOutputStream(), persons);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        final Person person = om.readValue(req.getInputStream(), Person.class);
        final Person createdPerson = personService.save(person);

        resp.setContentType("application/json");
        om.writeValue(resp.getOutputStream(), createdPerson);
    }
}
