package group.spd.arch.person;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;

public class FilePersonRepository implements PersonRepository, PersonProvider {

    private final ObjectMapper om;
    private final File db;

    public FilePersonRepository(ObjectMapper om) {
        this.om = om;
        db = new File("/tmp/db/person.json");
        if (!db.exists()) {
            try {
                db.getParentFile().mkdirs();
                db.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Person save(Person person) {
        final List<Person> persons = new ArrayList<>(getAll());
        person.setId(persons.size() + 1);
        persons.add(person);
        try {
            om.writeValue(db, persons);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return person;
    }

    @Override
    public List<Person> getAll() {
        final TypeReference<List<Person>> reference = new TypeReference<>() {
        };
        try {
            return om.readValue(db, reference);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return emptyList();
        }
    }

    @Override
    public Optional<Person> getById(int id) {
        return getAll().stream()
                .filter(person -> person.getId() == id)
                .findAny();
    }
}
