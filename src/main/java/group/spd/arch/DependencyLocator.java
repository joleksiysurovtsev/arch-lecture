package group.spd.arch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import group.spd.arch.notification.NotificationService;
import group.spd.arch.notification.NotificationServiceImpl;
import group.spd.arch.person.DbPersonRepository;
import group.spd.arch.person.PersonService;
import group.spd.arch.task.DbTaskRepository;
import group.spd.arch.task.TaskRepository;
import group.spd.arch.task.TaskService;

import javax.sql.DataSource;

public final class DependencyLocator {

    public static final DependencyLocator INSTANCE = new DependencyLocator();

    private final ObjectMapper om = buildObjectMapper();

    private final PersonService personService;
    private final TaskService taskService;

    private DependencyLocator() {
        final DataSource dataSource = buildDataSource();

        final DbPersonRepository personRepository = new DbPersonRepository(dataSource);
        final TaskRepository taskRepository = new DbTaskRepository(dataSource);

        final NotificationService notificationService = new NotificationServiceImpl();
        personService = new PersonService(personRepository);
        taskService = new TaskService(taskRepository, personRepository, notificationService);
    }

    public ObjectMapper getObjectMapper() {
        return om;
    }

    public PersonService getPersonService() {
        return personService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    private static ObjectMapper buildObjectMapper() {
        final ObjectMapper om = new ObjectMapper();
        om.registerModule(new JavaTimeModule());
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return om;
    }

    private static DataSource buildDataSource() {
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/arch");
        config.setUsername("arch-user");
        config.setPassword("arch-pass");
        config.setMaximumPoolSize(20);

        return new HikariDataSource(config);
    }
}
