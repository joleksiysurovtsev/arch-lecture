package group.spd.arch.person;

import group.spd.arch.error.ValidationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PersonServiceTest {

	@Mock
	private PersonRepository personRepository;

	@InjectMocks
	private PersonService personService;

	@Test
	@DisplayName("Should return persons from repository")
	void testGetAll() {
		final List<Person> expectedPersons = List.of(new Person());
		when(personRepository.getAll()).thenReturn(expectedPersons);

		final List<Person> persons = personService.getAll();
		assertSame(expectedPersons, persons);
	}

	@Test
	@DisplayName("Should fail validation for empty name")
	void validateNameOnSave() {
		final Person person = validPerson();
		person.setName("    ");
		assertThrows(ValidationException.class, () -> personService.save(person));
	}

	@Test
	@DisplayName("Should fail validation for empty email")
	void validateEmailOnSave() {
		final Person person = validPerson();
		person.setEmail("    ");
		assertThrows(ValidationException.class, () -> personService.save(person));
	}

	@Test
	@DisplayName("Should save valid person")
	void testSave() {
		final Person person = validPerson();
		personService.save(person);
		verify(personRepository).save(person);
	}

	private Person validPerson() {
		final Person person = new Person();
		person.setName("John Doe");
		person.setEmail("some@mail.com");
		return person;
	}
}